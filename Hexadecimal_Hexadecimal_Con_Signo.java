/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package hexadecimal_con_signo;

import javax.swing.JOptionPane;

/**
 *
 * @author German
 */
public class Hexadecimal_Hexadecimal_Con_Signo {

    public String Almacena[];
    public String Tabla[];

    public Hexadecimal_Hexadecimal_Con_Signo() {
        Tabla = new String[16];
        Tabla[0] = "0000";
        Tabla[1] = "0001";
        Tabla[2] = "0010";
        Tabla[3] = "0011";
        Tabla[4] = "0100";
        Tabla[5] = "0101";
        Tabla[6] = "0110";
        Tabla[7] = "0111";
        Tabla[8] = "1000";
        Tabla[9] = "1001";
        Tabla[10] = "1010";
        Tabla[11] = "1011";
        Tabla[12] = "1100";
        Tabla[13] = "1101";
        Tabla[14] = "1110";
        Tabla[15] = "1111";
    }

    public int hexadecimal_decimal(String hexadecimal) {
        String digitos = "0123456789ABCDEF";
        hexadecimal = hexadecimal.toUpperCase();//Aclarando que si el usuario agrega un digito en minuscula..el programa lo convierte a mayuscula
        int decimal = 0;
        for (int i = 0; i < hexadecimal.length(); i++) {
            char caracter = hexadecimal.charAt(i);
            int recorr = digitos.indexOf(caracter);
            decimal = 16 * decimal + recorr;
        }
        return decimal;
    }

    public void calc(String hexa) {
        int prueba = hexadecimal_decimal(hexa);
        int statico = 15;
        int total = prueba - statico;
        System.out.println(conversion_metodo_2(total));


    }

    public String conversion_metodo_2(int Decimal) {
        String Digitos_Hexadecimales = "0123456789ABCDEF";
        if (Decimal == 0) {
            return "0";
        }

        String hexadecimal = "";
        while (Decimal > 0) {
            int digito = Decimal % 16;
            hexadecimal = Digitos_Hexadecimales.charAt(digito) + hexadecimal;
            Decimal = Decimal / 16;
        }
        return hexadecimal;
    }

    public String convierte_hexa_binario(String he) {
        StringBuilder nuevo = new StringBuilder();
        StringBuilder e = new StringBuilder();
        for (int i = 0; i < he.length(); i++) {
            if ("0".equals(he.substring(i, i + 1))) {
                e.append("0000");

            }
            if ("1".equals(he.substring(i, i + 1))) {

                e.append("0001");
            }
            if ("2".equals(he.substring(i, i + 1))) {

                e.append("0010");
            }
            if ("3".equals(he.substring(i, i + 1))) {

                e.append("0011");
            }
            if ("4".equals(he.substring(i, i + 1))) {

                e.append("0100");
            }
            if ("5".equals(he.substring(i, i + 1))) {

                e.append("0101");
            }
            if ("6".equals(he.substring(i, i + 1))) {

                e.append("0110");
            }
            if ("7".equals(he.substring(i, i + 1))) {

                e.append("0111");
            }
            if ("8".equals(he.substring(i, i + 1))) {
                e.append("1000");
            }
            if ("9".equals(he.substring(i, i + 1))) {

                e.append("1001");
            }
            if ("A".equals(he.substring(i, i + 1))) {

                e.append("1010");
            }
            if ("B".equals(he.substring(i, i + 1))) {

                e.append("1011");
            }
            if ("C".equals(he.substring(i, i + 1))) {

                e.append("1100");
            }
            if ("D".equals(he.substring(i, i + 1))) {

                e.append("1101");
            }
            if ("E".equals(he.substring(i, i + 1))) {

                e.append("1110");
            }
            if ("F".equals(he.substring(i, i + 1))) {

                e.append("1111");
            }
            // String a=e+"";
    /*System.out.println(e); 
             String a=e+"";
             String inverso=Suma_Binaria(e+"", "1");
             System.out.println(inverso); 
             conversion(inverso);*/
            // return e+"";   StringBuilder nuevo = new  StringBuilder();

            /*for(int p=0;p<a.length();p++)
             {
             if("0".equals(a.substring(p, p+1)))
             {
             nuevo.append("1");   
             }else if("1".equals(a.substring(i, i+1)))
             {
             nuevo.append("0");
             }
               
             }*/
            System.out.println(e);
        }
        return e + "";
    }

    public String Suma_Binaria(String binario1, String binario2) {
        if (binario1 == null || binario2 == null) {
            return "";
        }
        int primer_elemento = binario1.length() - 1;
        int segundo_elemento = binario2.length() - 1;
        StringBuilder sb = new StringBuilder();
        int acarreado = 0;
        while (primer_elemento >= 0 || segundo_elemento >= 0) {
            int sum = acarreado;
            if (primer_elemento >= 0) {
                sum += binario1.charAt(primer_elemento) - '0';
                primer_elemento--;
            }
            if (segundo_elemento >= 0) {
                sum += binario2.charAt(segundo_elemento) - '0';
                segundo_elemento--;
            }
            acarreado = sum >> 1;
            sum = sum & 1;
            sb.append(sum == 0 ? '0' : '1');
        }
        if (acarreado > 0) {

            sb.append('1');
        }

        sb.reverse();

        return String.valueOf(sb);
    }

    public void conversion(String binario) {
        // String binario= JOptionPane.showInputDialog("Ingrese Porfavor la serie de bits");
        try {
            Almacena = new String[binario.length() + 1];

            for (int i = 0; i < binario.length(); i++) {
                Almacena[i] = "0";
            }

            for (int i = 0; i < binario.length(); i++) {

                Almacena[i] = binario.substring(i, i + 4);
                i++;
                i++;
                i++;

            }
            for (int i = 0; i < binario.length(); i++) {



                if (Almacena[i] == null ? Tabla[0] == null : Almacena[i].equals(Tabla[0])) {
                    System.out.print("0");
                    i++;
                    i++;
                    i++;
                }
                if (Almacena[i] == null ? Tabla[1] == null : Almacena[i].equals(Tabla[1])) {
                    System.out.print("1");
                    i++;
                    i++;
                    i++;
                }
                if (Almacena[i] == null ? Tabla[2] == null : Almacena[i].equals(Tabla[2])) {
                    System.out.print("2");
                }
                if (Almacena[i] == null ? Tabla[3] == null : Almacena[i].equals(Tabla[3])) {
                    System.out.print("3");
                }
                if (Almacena[i] == null ? Tabla[4] == null : Almacena[i].equals(Tabla[4])) {
                    System.out.print("4");
                }
                if (Almacena[i] == null ? Tabla[5] == null : Almacena[i].equals(Tabla[5])) {
                    System.out.print("5");
                }
                if (Almacena[i] == null ? Tabla[6] == null : Almacena[i].equals(Tabla[6])) {
                    System.out.print("6");
                }
                if (Almacena[i] == null ? Tabla[7] == null : Almacena[i].equals(Tabla[7])) {
                    System.out.print("7");
                }
                if (Almacena[i] == null ? Tabla[8] == null : Almacena[i].equals(Tabla[8])) {
                    System.out.print("8");
                }
                if (Almacena[i] == null ? Tabla[9] == null : Almacena[i].equals(Tabla[9])) {
                    System.out.print("9");
                }
                if (Almacena[i] == null ? Tabla[10] == null : Almacena[i].equals(Tabla[10])) {
                    System.out.print("A");
                }
                if (Almacena[i] == null ? Tabla[11] == null : Almacena[i].equals(Tabla[11])) {
                    System.out.print("B");
                }
                if (Almacena[i] == null ? Tabla[12] == null : Almacena[i].equals(Tabla[12])) {
                    System.out.print("C");
                }
                if (Almacena[i] == null ? Tabla[13] == null : Almacena[i].equals(Tabla[13])) {
                    System.out.print("D");
                }
                if (Almacena[i] == null ? Tabla[14] == null : Almacena[i].equals(Tabla[14])) {
                    System.out.print("E");
                }
                if (Almacena[i] == null ? Tabla[15] == null : Almacena[i].equals(Tabla[15])) {
                    System.out.print("F");
                }

            }



            for (int i = 0; i < Almacena.length; i++) {
                //System.out.println(Almacena[i]);
            }

        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "Los bloques que ingreso no estan siendo ni byte,Word,Dword o Qword");

            // conversion();
        };


    }

    public void suma_generica(String hex) {
        String primero = convierte_hexa_binario(hex);

        System.out.println(primero);
        StringBuilder invertido = new StringBuilder();
        {
            for (int i = 0; i < primero.length(); i++) {
                if ("0".equals(primero.substring(i, i + 1))) {
                    invertido.append("1");
                } else if ("1".equals(primero.substring(i, i + 1))) {
                    invertido.append("0");
                }

            }
        }
        System.out.println(invertido);
        String segundo = Suma_Binaria(invertido + "", "1");
        System.out.println(segundo);
        conversion(segundo);

    }

    public static void main(String[] Args) {
        Hexadecimal_Hexadecimal_Con_Signo o = new Hexadecimal_Hexadecimal_Con_Signo();
        // o.calc("6A3D");
        //o.convierte_hexa_binario("6A3D"); System.out.println();
        System.out.println();
        //o.conversion("0110101000111101");
        System.out.println();
        o.suma_generica("95C3");
        System.out.println();
        //---------------------------------------->
        System.out.println();
        o.suma_generica("6A3D");
        System.out.println();

    }
}
